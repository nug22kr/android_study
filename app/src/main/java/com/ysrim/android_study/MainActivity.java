package com.ysrim.android_study;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button startBtn;
    FnClass fn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init();
        startBtn = (Button)findViewById(R.id.startBtn);
        fn = new FnClass(getApplicationContext());


        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fn.showMsg("시작버튼");
            }
        });
    }

}

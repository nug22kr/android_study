package com.ysrim.android_study;

import android.content.Context;
import android.widget.Toast;



public class FnClass {

    Context ctx;

    public FnClass(Context _ctx) {
        ctx = _ctx;
    }


    public void showMsg(String msg){
        Toast.makeText(ctx,msg,Toast.LENGTH_SHORT).show();
    }

    public void showMsg(String msg, Integer duration){
        Toast.makeText(ctx,msg,duration).show();
    }
}
